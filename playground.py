import mlrose_hiive as mlrose
import numpy as np
import curve


def main():
    curve.plot_curve(
        [
            [1, 2, 3],
            [2, 4, 6],
        ],
        [
            [1, 2, 3],
            [2, 4, 6],
        ],
        "x",
        "y",
        ["one", "two"],
        ["r", "b"],
        "title_here",
        "plots",
        "test",
    )
    # fitness = mlrose.Queens()
    # problem = mlrose.DiscreteOpt(
    #     length=8, fitness_fn=fitness, maximize=False, max_val=8
    # )
    # schedule = mlrose.ExpDecay()
    # init_state = np.array([0, 1, 2, 3, 4, 5, 6, 7])
    # best_state, best_fitness, fitness_curve = mlrose.simulated_annealing(
    #     problem,
    #     schedule=schedule,
    #     max_attempts=10,
    #     max_iters=1000,
    #     init_state=init_state,
    #     random_state=1,
    # )
    # print("The best state found is: ", best_state)
    # print("The best state fitness is: ", best_fitness)


if __name__ == "__main__":
    main()
