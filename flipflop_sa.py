import time

import mlrose_hiive as mlrose

import constants
import curve


def main():
    fitness_curves = plot_fitness_curve()


def plot_fitness_curve():
    results = {}

    for problem_size in constants.FLIP_FLOP_PROBLEM_SIZES:
        problem = mlrose.FlipFlopOpt(
            length=problem_size
        )
        problem.set_mimic_fast_mode(True)

        start = time.time()
        best_state, best_fitness, fitness_curve = mlrose.simulated_annealing(
            problem,
            max_attempts=constants.FLIP_FLOP_DEFAULT_MAX_ATTEMPT,
            max_iters=constants.FLIP_FLOP_DEFAULT_MAX_ITER,
            schedule=mlrose.GeomDecay(init_temp=500),
            random_state=10,
            curve=True,
        )
        elapsed = time.time() - start

        results[problem_size] = {
            "best_state": best_state,
            "best_fitness": best_fitness,
            "fitness_curve": fitness_curve,
            "problem_size": problem_size,
            "elapsed_seconds": elapsed,
        }

    fitness_curves = [
        results[problem_size]["fitness_curve"] for problem_size in results
    ]
    problem_sizes = [f"N={problem_size}" for problem_size in results]
    elapsed_times = [
        [results[problem_size]["elapsed_seconds"], problem_size]
        for problem_size in results
    ]

    curve.plot_curve(
        fitness_curves,
        "Fitness Score",
        "Iteration",
        problem_sizes,
        constants.COLORS,
        "Flip Flop Fitness Score (SA)",
        "flipflop_fitness_score-sa",
    )

    curve.plot_curve(
        [elapsed_times],
        "Time (Seconds)",
        "Problem Size (N)",
        ["Elapsed"],
        constants.COLORS,
        "Flip Flop Duration (SA)",
        "flipflop_duration-sa",
    )

    print("==== SUMMARY ==== ")
    for problem_size in results:
        result = results[problem_size]
        best_state = result["best_state"]
        best_fitness = result["best_fitness"]
        elapsed_seconds = result["elapsed_seconds"]
        print(f"N={problem_size}")
        print(f"best_fitness={best_fitness}")
        print(f"elapsed={elapsed_seconds}")
        print("\n")

    print(fitness_curves)
    return fitness_curves


if __name__ == "__main__":
    main()
