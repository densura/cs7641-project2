from datetime import datetime

import matplotlib.pyplot as plt


def plot_curve(
    data,
    ylabel,
    xlabel,
    labels,
    color_lis,
    title,
    file_name,
    dir="plots",
    save_file=True,
):
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.grid()

    for idx in range(len(data)):
        line_data = data[idx]
        x = [_[1] for _ in line_data]
        y = [_[0] for _ in line_data]
        label = labels[idx]
        color = color_lis[idx]
        plt.plot(x, y, "-", color=color, label=label, linewidth=0.5)
        plt.legend(loc="best")

    if save_file:
        file_path = "{}/{}_{}.png".format(dir, file_name, datetime.now())
        print(f"Saving {file_path}")
        plt.savefig(file_path, format="png")
    else:
        plt.show()

    plt.close()
