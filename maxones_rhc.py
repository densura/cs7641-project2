import time

import mlrose_hiive as mlrose
import numpy as np

import constants
import curve


def main():
    fitness = mlrose.OneMax()
    fitness_results = {}
    num_restarts_results = {}
    max_attempt_results = {}

    for problem_size in constants.MAX_ONES_PROBLEM_SIZES:
        problem = mlrose.DiscreteOpt(
            length=problem_size, fitness_fn=fitness, maximize=True, max_val=2
        )
        init_state = np.zeros([problem_size])
        start = time.time()
        best_state, best_fitness, fitness_curve = mlrose.random_hill_climb(
            problem,
            max_attempts=constants.MAX_ONES_DEFAULT_MAX_ATTEMPT,
            init_state=init_state,
            random_state=10,
            curve=True,
        )
        elapsed = time.time() - start

        fitness_results[problem_size] = {
            "best_state": best_state,
            "best_fitness": best_fitness,
            "fitness_curve": fitness_curve,
            "problem_size": problem_size,
            "elapsed_seconds": elapsed,
        }

    problem_size = 400
    for max_attempt in constants.MAX_ONES_MAX_ATTEMPTS:
        problem = mlrose.DiscreteOpt(
            length=problem_size, fitness_fn=fitness, maximize=True, max_val=2
        )
        init_state = np.zeros([problem_size])
        best_state, best_fitness, fitness_curve = mlrose.random_hill_climb(
            problem,
            max_attempts=max_attempt,
            init_state=init_state,
            random_state=10,
            curve=True,
        )
        max_attempt_results[max_attempt] = {
            "best_state": best_state,
            "best_fitness": best_fitness,
            "fitness_curve": fitness_curve,
        }

    for num_restart in constants.MAX_ONES_NUM_RESTARTS:
        problem = mlrose.DiscreteOpt(
            length=problem_size, fitness_fn=fitness, maximize=True, max_val=2
        )
        init_state = np.zeros([problem_size])
        best_state, best_fitness, fitness_curve = mlrose.random_hill_climb(
            problem,
            max_attempts=constants.MAX_ONES_DEFAULT_MAX_ATTEMPT,
            restarts=num_restart,
            init_state=init_state,
            random_state=10,
            curve=True,
        )
        num_restarts_results[num_restart] = {
            "best_state": best_state,
            "best_fitness": best_fitness,
            "fitness_curve": fitness_curve,
        }

    # Plot num restarts vs. fitness curve
    num_restart_fitness = [
        [num_restarts_results[num_restart]["best_fitness"], num_restart]
        for num_restart in num_restarts_results
    ]
    curve.plot_curve(
        [num_restart_fitness],
        "Fitness Score",
        "Num Restarts",
        ["NumRestart"],
        constants.COLORS,
        "Max Ones Fitness Score by Num Restart (RHC)",
        "maxones_num_restart_fitness_score-rhc",
    )

    # Plot max attempt vs. fitness curve
    max_attempt_fitness = [
        [max_attempt_results[max_attempt]["best_fitness"], max_attempt]
        for max_attempt in max_attempt_results
    ]
    curve.plot_curve(
        [max_attempt_fitness],
        "Fitness Score",
        "Max Attempt",
        ["MaxAttempt"],
        constants.COLORS,
        "Max Ones Fitness Score by Max Attempt (RHC)",
        "maxones_max_attempt_fitness_score-rhc",
    )

    # Plot problem size vs. fitness curve
    problem_sizes = [f"N={problem_size}" for problem_size in fitness_results]
    fitness_curves = [
        fitness_results[problem_size]["fitness_curve"] for problem_size in fitness_results
    ]
    curve.plot_curve(
        fitness_curves,
        "Fitness Score",
        "Iteration",
        problem_sizes,
        constants.COLORS,
        "Max Ones Fitness Score (RHC)",
        "maxones_fitness_score-rhc",
    )

    # Plot problem size vs. duration
    elapsed_times = [
        [fitness_results[problem_size]["elapsed_seconds"], problem_size]
        for problem_size in fitness_results
    ]
    curve.plot_curve(
        [elapsed_times],
        "Time (Seconds)",
        "Problem Size (N)",
        ["Elapsed"],
        constants.COLORS,
        "Max Ones Duration (RHC)",
        "maxones_duration-rhc",
    )

    print("==== SUMMARY ==== ")
    for problem_size in fitness_results:
        result = fitness_results[problem_size]
        best_state = result["best_state"]
        best_fitness = result["best_fitness"]
        elapsed_seconds = result["elapsed_seconds"]
        print(f"N={problem_size}")
        # print(f"best_state={best_state}")
        print(f"best_fitness={best_fitness}")
        print(f"elapsed={elapsed_seconds}")
        print("\n")

    print("max_attempt_fitness")
    print(max_attempt_fitness)
    print("elapsed_times")
    print(elapsed_times)
    print("num_restart_fitness")
    print(num_restart_fitness)

    return fitness_results


if __name__ == "__main__":
    main()
