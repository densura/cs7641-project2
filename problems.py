import mlrose_hiive as mlrose
import numpy as np
from mlrose_hiive import UniformCrossOver, ChangeOneMutator, DiscreteOpt

"""
Fibonacci sequence:  1, 1, 2, 3, 5, 8,
Score(01001110000011111111) = 
"""


def longest_fib_string(state):
    bit = state[0]
    a = 0
    b = 1

    idx = 0
    score = 0

    while idx < len(state):
        x = b  # The length of the next segment.
        count = 0

        # See how many bits of the next segment we get.
        while count < x and idx < len(state):
            if state[idx] is bit:
                score += 1
                count += 1
                idx += 1
            else:
                return score

        c = a + b
        a = b
        b = c

        bit ^= 1

    return score


class LongestFibonacciString:
    def __init__(self):
        self.prob_type = 'discrete'

    def evaluate(self, state):
        return longest_fib_string(state)

    def get_prob_type(self):
        return 'discrete'


class LongestFibonacciStringOpt(DiscreteOpt):
    def __init__(self, length, crossover=None, mutator=None):
        self.length = length
        fitness_fn = LongestFibonacciString()
        maximize = True

        self.max_val = 2
        crossover = UniformCrossOver(self) if crossover is None else crossover
        mutator = ChangeOneMutator(self) if mutator is None else mutator
        super().__init__(length, fitness_fn, maximize, length, crossover, mutator)

        state = np.random.randint(self.length, size=self.length)
        np.random.shuffle(state)
        self.set_state(state)

    def get_prob_type(self):
        return 'discrete'

    def can_stop(self):
        return int(self.get_fitness()) == self.length


if __name__ == "__main__":

    tests = [
        [[0, 1], 2],
        [[1, 0], 2],
        [[0, 1, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0], 12],
        [[0, 1, 0, 0, 1, 1, 1, 0, 1, 0, 0, 0], 8],
        [[0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0], 2],
    ]

    for test in tests:
        if longest_fib_string(test[0], {}) == test[1]:
            print("PASS: {}".format(test[0]))
        else:
            print("FAIL: {}".format(test[0]))
