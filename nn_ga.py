import time

import mlrose_hiive as mlrose
import numpy as np
from sklearn.metrics import f1_score

import constants
import curve
import segmentation_dataset
from nncurve import show_learning_curve

SMALL_DATA_MODE = False
HIDDEN_NODES = [25, 25]
MAX_ITER = 20000


def plot_learing_curve(X_train, y_train):
    show_learning_curve(
        create_classifier(),
        X_train,
        y_train,
        np.linspace(0.1, 1.0, 10),
        "Neural Network Learning Curve (GA)",
        ylabel="f1_macro_score",
        file_prefix="ga",
        scoring="f1_macro",
        cv=3,
    )


def plot_fitness_curve(fitness_curve):
    curve.plot_curve(
        [fitness_curve],
        "Fitness Score",
        "Iteration",
        ["GA Fitness"],
        constants.COLORS,
        "",
        "nn_fitness_score-ga",
    )


def main():
    X_train, X_test, y_train, y_test = segmentation_dataset.load_train_test_data(small=SMALL_DATA_MODE)

    print("\n== Plot Learning Curve ==")
    plot_learing_curve(X_train, y_train)

    print("\n== Training ==")
    start = time.time()
    clf = create_classifier()
    _ = clf.fit(X_train, y_train)
    elapsed = time.time() - start

    print("Training Time: {}".format(elapsed))

    fitted_weights = clf.fitted_weights
    loss = clf.loss
    fitness_curve = clf.fitness_curve

    plot_fitness_curve(clf.fitness_curve)

    y_train_pred = clf.predict(X_train)
    y_train_f1_score = f1_score(y_train, y_train_pred, average="macro")

    y_test_pred = clf.predict(X_test)
    y_test_f1_score = f1_score(y_test, y_test_pred, average="macro")

    print("\n== Fitness Curve ==")
    print(fitness_curve)

    print("\n== Fitted Weights ==")
    print(fitted_weights)

    print("\n== Loss ==")
    print(loss)

    print("\n== Train F1 Score ==")
    print(y_train_f1_score)

    print("\n== F1 Score ==")
    print(y_test_f1_score)

    print("\n== DONE ==")


def create_classifier():
    return mlrose.NeuralNetwork(
        hidden_nodes=HIDDEN_NODES,
        learning_rate=0.1,
        early_stopping=True,
        activation='relu',
        algorithm='genetic_alg',
        max_iters=MAX_ITER,
        mutation_prob=0.4,
        max_attempts=100,
        random_state=10,
        curve=True
    )


if __name__ == "__main__":
    main()
