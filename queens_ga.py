import time

import mlrose_hiive as mlrose
import problems

import curve

STARTING_SIZE = 8
ENDING_SIZE = 12
# PROBLEM_SIZES = [32, 64, 128, 256]
PROBLEM_SIZES = [4, 8, 16, 32, 64]
COLORS = [
    (1, 0, 0),
    (0, 1, 0),
    (0, 0, 1),
    (0.3, 0.3, 0.3),
]


def main():
    results = {}

    def state_fitness_callback(
            iteration=None,
            attempt=None,
            done=None,
            state=None,
            fitness=None,
            fitness_evaluations=None,
            curve=None,
            user_data=None,
    ):
        print("Iteration: {} | Attempt: {} | State: {}".format(iteration, attempt, state))
        return True

    for problem_size in PROBLEM_SIZES:
        problem = mlrose.QueensOpt(
            length=problem_size,
            maximize=False,
        )
        start = time.time()
        best_state, best_fitness, fitness_curve = mlrose.genetic_alg(
            problem,
            max_attempts=20,
            max_iters=1000,
            random_state=10,
            curve=True,
            state_fitness_callback=state_fitness_callback
        )
        elapsed = time.time() - start

        results[problem_size] = {
            "best_state": best_state,
            "best_fitness": best_fitness,
            "fitness_curve": fitness_curve,
            "problem_size": problem_size,
            "elapsed_seconds": elapsed,
        }

    fitness_curves = [
        results[problem_size]["fitness_curve"] for problem_size in results
    ]
    problem_sizes = [f"N={problem_size}" for problem_size in results]
    elapsed_times = [
        [results[problem_size]["elapsed_seconds"], problem_size]
        for problem_size in results
    ]

    curve.plot_curve(
        fitness_curves,
        "Fitness Score",
        "Iteration",
        problem_sizes,
        COLORS,
        "Quees Fitness Score",
        "queens_fitness_score-ga",
    )

    curve.plot_curve(
        [elapsed_times],
        "Time (Seconds)",
        "Problem Size (N)",
        ["Elapsed"],
        COLORS,
        "Queens Duration",
        "queens_duration-ga",
    )

    print("==== SUMMARY ====")
    for problem_size in results:
        result = results[problem_size]
        best_state = result["best_state"]
        best_fitness = result["best_fitness"]
        elapsed_seconds = result["elapsed_seconds"]
        print(f"N={problem_size}")
        print(f"best_state={best_state}")
        print(f"best_fitness={best_fitness}")
        print(f"elapsed={elapsed_seconds}")
        print("\n")

    return results


if __name__ == "__main__":
    main()
