import time

import mlrose_hiive as mlrose
import numpy as np

import constants
import curve

MAX_ITER = 10000
MAX_ATTEMPT = 100
PROBLEM_SIZE = 400


def main():
    results = run_all()
    plot_fitness_curve(results)


def state_fitness_callback(iteration=None,
                           attempt=None,
                           done=None,
                           state=None,
                           fitness=None,
                           fitness_evaluations=None,
                           curve=None,
                           user_data=None):
    if fitness_evaluations >= MAX_ITER:
        return False
    return True


def plot_fitness_curve(results):
    fitness_curves = [
        results['rhc']["fitness_curve"],
        results['sa']["fitness_curve"],
        results['ga']["fitness_curve"],
        results['mimic']["fitness_curve"],
    ]
    curve.plot_curve(
        data=fitness_curves,
        ylabel="Fitness Score",
        xlabel="Iteration",
        labels=["RHC", "SA", "GA", "MIMIC"],
        color_lis=constants.COLORS,
        title="Knapsack Fitness Score",
        file_name="knapsack_fitness_all_algo"
    )
    print("=== SUMMARY ===")
    for algo in results:
        result = results[algo]
        print(algo)
        print("best_fitness: {}".format(result["best_fitness"]))
        print("elapsed_seconds: {}".format(result["elapsed_seconds"]))
        print("\n")


def run_sa(problem, problem_size):
    init_state = np.zeros([problem_size])
    start = time.time()
    best_state, best_fitness, fitness_curve = mlrose.simulated_annealing(
        problem,
        schedule=mlrose.ExpDecay(),
        max_attempts=MAX_ATTEMPT,
        init_state=init_state,
        random_state=10,
        curve=True,
        state_fitness_callback=state_fitness_callback,
        callback_user_info=[],
    )
    elapsed = time.time() - start
    return {
        "best_state": best_state,
        "best_fitness": best_fitness,
        "fitness_curve": fitness_curve,
        "elapsed_seconds": elapsed,
    }


def run_rhc(problem, problem_size):
    init_state = np.zeros([problem_size])
    start = time.time()

    best_state, best_fitness, fitness_curve = mlrose.random_hill_climb(
        problem,
        max_attempts=MAX_ATTEMPT,
        init_state=init_state,
        random_state=10,
        curve=True,
        state_fitness_callback=state_fitness_callback,
        callback_user_info=[],
    )
    elapsed = time.time() - start
    return {
        "best_state": best_state,
        "best_fitness": best_fitness,
        "fitness_curve": fitness_curve,
        "elapsed_seconds": elapsed,
    }


def run_mimic(problem):
    start = time.time()
    problem.set_mimic_fast_mode(True)
    best_state, best_fitness, fitness_curve = mlrose.mimic(
        problem,
        max_attempts=MAX_ATTEMPT,
        max_iters=MAX_ITER,
        random_state=10,
        curve=True,
        state_fitness_callback=state_fitness_callback,
        callback_user_info=[],
    )
    elapsed = time.time() - start
    return {
        "best_state": best_state,
        "best_fitness": best_fitness,
        "fitness_curve": fitness_curve,
        "elapsed_seconds": elapsed,
    }


def run_ga(problem):
    start = time.time()
    best_state, best_fitness, fitness_curve = mlrose.genetic_alg(
        problem,
        max_attempts=MAX_ATTEMPT,
        max_iters=MAX_ITER,
        random_state=10,
        curve=True,
        state_fitness_callback=state_fitness_callback,
        callback_user_info=[],
    )
    elapsed = time.time() - start
    return {
        "best_state": best_state,
        "best_fitness": best_fitness,
        "fitness_curve": fitness_curve,
        "elapsed_seconds": elapsed,
    }


def run_all():
    problem_size = PROBLEM_SIZE
    results = {
        'sa': run_sa(create_problem(problem_size), problem_size),
        'ga': run_ga(create_problem(problem_size)),
        'rhc': run_rhc(create_problem(problem_size), problem_size),
        'mimic': run_mimic(create_problem(problem_size)),
    }
    return results


def create_problem(problem_size):
    return mlrose.KnapsackOpt(length=problem_size,
                              values=np.random.randint(1, 10, problem_size),
                              weights=np.random.randint(1, 10, problem_size),
                              max_weight_pct=0.5
                              )


if __name__ == "__main__":
    main()
