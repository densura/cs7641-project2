NOW := $(shell date "+%Y-%m-%d_%H:%M:%S")

maxones_summary:
	python3 maxones_summary.py > output/maxones_summary_$(NOW).out

maxones_rhc:
	python3 maxones_rhc.py > output/maxones_rhc_$(NOW).out

maxones_sa:
	python3 maxones_sa.py > output/maxones_sa_$(NOW).out

maxones_ga:
	python3 maxones_ga.py > output/maxones_ga_$(NOW).out

maxones_mimic:
	python3 maxones_mimic.py > output/maxones_mimic_$(NOW).out

flipflop_summary:
	python3 flipflop_summary.py > output/flipflop_summary_$(NOW).out

flipflop_mimic:
	python3 flipflop_mimic.py > output/flipflop_mimic_$(NOW).out

flipflop_ga:
	python3 flipflop_ga.py > output/flipflop_ga_$(NOW).out

flipflop_sa:
	python3 flipflop_sa.py > output/flipflop_sa_$(NOW).out

flipflop_rhc:
	python3 flipflop_rhc.py > output/flipflop_rhc_$(NOW).out

queens_mimic:
	python3 queens_mimic.py > output/queens_mimic_$(NOW).out

knapsack_mimic:
	python3 knapsack_mimic.py > output/knapsack_mimic_$(NOW).out

knapsack_rhc:
	python3 knapsack_rhc.py > output/knapsack_rhc_$(NOW).out

knapsack_ga:
	python3 knapsack_ga.py > output/knapsack_ga_$(NOW).out

knapsack_sa:
	python3 knapsack_sa.py > output/knapsack_sa_$(NOW).out

knapsack_summary:
	python3 knapsack_summary.py > output/knapsack_summary_$(NOW).out

nn_rhc:
	python3 nn_rhc.py > output/nn_rhc_$(NOW).out

nn_sa:
	python3 nn_sa.py > output/nn_sa_$(NOW).out

nn_ga:
	python3 nn_ga.py > output/nn_ga_$(NOW).out