import pandas as pd
import numpy as np
from sklearn.preprocessing import OneHotEncoder, StandardScaler, OrdinalEncoder
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle

"""
REGION-CENTROID-COL,REGION-CENTROID-ROW,REGION-PIXEL-COUNT,SHORT-LINE-DENSITY-5,SHORT-LINE-DENSITY-2,VEDGE-MEAN,VEDGE-SD,HEDGE-MEAN,HEDGE-SD,INTENSITY-MEAN,RAWRED-MEAN,RAWBLUE-MEAN,RAWGREEN-MEAN,EXRED-MEAN,EXBLUE-MEAN,EXGREEN-MEAN,VALUE-MEAN,SATURATION-MEAN,HUE-MEAN
"""
COLUMN_NAMES = {
    0: "IMAGE-CLASS",
    1: "REGION-CENTROID-COL",
    2: "REGION-CENTROID-ROW",
    3: "REGION-PIXEL-COUNT",
    4: "SHORT-LINE-DENSITY-5",
    5: "SHORT-LINE-DENSITY-2",
    6: "VEDGE-MEAN",
    7: "VEDGE-SD",
    8: "HEDGE-MEAN",
    9: "HEDGE-SD",
    10: "INTENSITY-MEAN",
    11: "RAWRED-MEAN",
    12: "RAWBLUE-MEAN",
    13: "RAWGREEN-MEAN",
    14: "EXRED-MEAN",
    15: "EXBLUE-MEAN",
    16: "EXGREEN-MEAN",
    17: "VALUE-MEAN",
    18: "SATURATION-MEAN",
    19: "HUE-MEAN",
}

TEST_SET_SIZE = 0.1


def load_train_test_data(small=False):
    df = pd.read_csv("./dataset/segmentation.data.combined", header=None)
    df = df.rename(COLUMN_NAMES, axis=1)
    df = shuffle(df, random_state=10)

    X = df.drop(["IMAGE-CLASS", "REGION-PIXEL-COUNT"], axis=1)
    y = df[["IMAGE-CLASS"]]

    scaler = StandardScaler()
    scaled_X = scaler.fit_transform(X)
    X = pd.DataFrame(scaled_X, columns=X.columns)

    X_train, X_test, y_train, y_test = train_test_split(
        X, y, test_size=TEST_SET_SIZE, random_state=10, stratify=y
    )

    one_hot = OneHotEncoder()

    y_train_hot = one_hot.fit_transform(y_train.values.ravel().reshape(-1, 1)).todense()
    y_test_hot = one_hot.transform(y_test.values.ravel().reshape(-1, 1)).todense()

    if small:
        return X_train[:200], X_test[:200], y_train_hot[:200], y_test_hot[:200]
    else:
        return X_train, X_test, y_train_hot, y_test_hot


if __name__ == "__main__":
    X_train, X_test, y_train, y_test = load_train_test_data()
    print(X_train.shape)
    print(y_train.shape)
    print(X_test.shape)
    print(y_test.shape)
