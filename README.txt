Account: dsuratna3
Name: Dennis Suratna
Code and dataset are located at: https://gitlab.com/densura/cs7641-project2.git

To clone the repository, run: `git clone https://gitlab.com/densura/cs7641-project2.git`

The code was written using python 3.9. To install the dependencies, run the follwoing command: `pip3 install -r requirements.txt`

Each problem and algorithm pair is contained within a single python program. Plots and program outputs are stored under `/plots` and `/outputs` respectively.
I have added a Makefile to make running code easier.

```
make maxones_ga # Run Max Ones with Genetic algorithm
make maxones_sa # Run Max Ones with Simulated Annearling
make maxones_rhc # Run Max Ones with Randomized Hill Climb
make maxones_mimic # Run Max Ones with MIMIC
make maxones_summary # Fitness plots for all algorithms

make flipflop_ga # Run Flip Flop with Genetic algorithm
make flipflop_sa # Run Flip Flop with Simulated Annearling
make flipflop_rhc # Run Flip Flop with Randomized Hill Climb
make flipflop_mimic # Run Flip Flop problem with MIMIC
make flipflop_summary # Fitness plots for all algorithms

make knapsack_ga # Run Knapsack with Genetic algorithm
make knapsack_sa # Run Knapsack with Simulated Annealing
make knapsack_rhc # Run Knapsack with Randomized Hill Climb
make knapsack_mimic # Run Knapsack problem with MIMIC
make knapsack_summary # Fitness plots for all algorithms

make nn_sa # Run Neural Network with simulated annealing
make nn_ga # Run Neural Network with genetic algorithm
make nn_rhc # Run Neural Network with randomized hill climb
```

